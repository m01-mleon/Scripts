#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 4.- Dias del mes per arg rebut
# --------------------------- 
# validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
	echo "Error numero arguments"
	echo "Usage: $0 numero... (1-12)"
	exit $ERR_ARG
fi
# validar numero arg
for mes in $@
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]; then
		echo "Error numero de l'argument (1-12)" >> /dev/stderr
# sortida dias mes
	else
		case $mes in
			"2")
				dies=28;;
			"4"|"6"|"9"|"11")
				dies=30;;
			*)
				dies=31;;
		esac
		echo "El mes $mes te $dies dies"
	fi
done
exit 0
