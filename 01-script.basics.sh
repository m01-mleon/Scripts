#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 1.- Mostrar l'entrada estàndard numerant línia a línia.
# --------------------------- 
num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0
