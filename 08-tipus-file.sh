#! /bin/bash
# Marcos León Correa
# Febrer 2024
#
# indicar si dir es regular, dir, link o altres
# ---------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
#symbolic link -h
#regular file -f
#dir -d
# 1) requisits arg
if [ $# -ne 1 ]
then
	echo "err $1 arg invalid, usage: $0 'ruta'"
	exit $ERR_ARGS
fi
# 2) verif file
fit=$1
if [ ! -e $fit  ]; then
	echo "$fit no existeix"
	exit $ERR_NOEXIST
elif [ -f $fit  ]; then
	echo "$fit es un regular file"
elif [ -h $fit ]; then
	echo "$fit es un link"
elif [ -d $fit ]; then
	echo "e$fit es un directori"
else
	echo "$fit es una altra cosa"
fi
exit 0
