#! /bin/bash
#
# Marcos León Correa
# Febrer 2024
#
#Validar nota: suspes - aprovat
#-------------------------------
ERR_NARGS=1
ERR_NOTA=2
#1) Requisits
if [ $# -ne 1 ] 
then
	echo "arg invalid: Usage $0 'nota'"
	exit $ERR_NARGS
fi
#2) validacio arg
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
	echo "$1 nota no valida, nota [0-10]. Usage: $0 'nota'"
	exit $ERR_NOTA
fi
#3) Xixa
if [ $1 -ge 5 ]
then
	echo "Aprovat"
else
	echo "Suspes"
fi
exit 0

