#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 5.- Linia a linia entrada estandard retallant 50 primers caracters
# ---------------------------
while read -r line
do
	echo "$line" | cut -c-50
done
exit 0
