#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 3.- Comptador desde 0 fins valor indicat
# --------------------------- 
# validar args
ERR_ARG=1
if [ $# -ne 1 ];then
	echo "Error numero arguments"
	echo "Usage: $0 numero"
	exit $ERR_ARG
fi
# comptador fins valor indicat
MAX=$1
comptador=0
while [ $comptador -le $MAX ]
do
	echo "$comptador"
	((comptador++))
done
exit 0
