#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 9.- mostrar user si existeix amb stdin
# --------------------------- 
#noms user
while read -r user
do
	grep -iq "^$user:" /etc/passwd
	if [ $? -eq 0 ];then
	        echo "$user"
	else 
        	echo "$user no es usuari" >> /dev/stderr
    	fi
done
exit 0
