#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 8.- mostrar user si existeix amb arg
# --------------------------- 
#validar arg
ERR_ARGS=1
if [ $# -eq 0 ]; then
	echo "Error numero arguments"
	echo "Usage: $0 arg..."
	exit $ERRARGS
fi
#noms user
for user in $*
do
	grep -iq "^$user:" /etc/passwd
	if [ $? -eq 0 ];then
	        echo "$user"
	else 
        	echo "$user no es usuari" >> /dev/stderr
    	fi
done
exit 0
