#! /bin/bash
# Marcos León Correa
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 2.- Mostrar els arguments rebuts línia a línia, tot numerant-los.
# ---------------------------
# validar args
ERR_ARGS=1
if [ $# -eq 0 ]; then
	echo "Error numero arguments"
	echo "Usage: $0 arg..."
	exit $ERRARGS
fi
# numerar args
num=1
for arg in $*
do
	echo "$num: $arg"
	((num++))
done
exit 0
