#! /bin/bash
# Marcos León Correa
#
# Febrer 2024
#
# Exemples case
# ------------------- 
# 2)
case $1 in
	"dl"|"dt"|"dc"|"dj"|"dv")
		echo "$1 es un dia laborble"
		;;
	"ds"|"dm")
		echo "$1 es un dia festiu"
		;;
	*)
		echo "això $1 no es un dia"
esac
exit 0
# 1) exemple vocals
case $1 in
	[aeiou])
		echo "$1 és una vocal"
		;;
	[bcdfghjklmnñpqrstvwxyz])
		echo "$1 és una consonant"
		;;
	*)
		echo "$1 és una altra cosa"
		;;
esac
exit 0
