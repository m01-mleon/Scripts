#! /bin/bash
# Marcos León Correa
# Febrer 2024
# 12 for notes
# ---------------------
# 1) validar args
if [ $# -eq 0  ]; then
	echo "Error num args"
	echo "Usage: $0 nota..."
	exit 1
fi
# 2)
for nota in $*
do
	if ! [ $nota -ge 0 -a $nota -le 10 ]; then
		echo "Error: nota $nota no valida (0-10)"
			>> /dev/stderr
	else
		if [ $nota -lt 5 ];then
			echo "suspes"
		elif [ $nota -ge 7 ];then
			echo "aprovat"
		else
			echo "excelent"
		fi
	fi
done
exit 0
