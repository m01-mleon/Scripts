#! /bin/bash
#Marcos León Correa
#Febrer 2024
#
#Exemples de if.
#--------------------
#
#1) Validar Arguments
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

#2) Xixa
edat=$1
if [ $edat -lt 18 ]
then
	echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
	echo "edat $Edat és edat activa"
else
	echo "edat $edat és edat de jubilació"
fi
exit 0
