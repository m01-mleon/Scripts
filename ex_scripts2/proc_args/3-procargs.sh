#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 3-Processar arguments que són matricules: 
# a) Llistar les vàlides, del tipus: 9999-AAA. 
# b) stdout les que són vàlides, per stderr les no vàlides. 
# Retorna de status el número d’errors (de no vàlides).
# --------------------------- 
count=0
for matricula in "$@"
do
	echo "$matricula" | grep -e "^[0-9]{4} [A-Z]{3}$"
	if [ $? -ne 0 ];then
		echo "Error"
		((count++))
	fi
done
exit $count
