#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 1-Processar els arguments i mostrar per stdout només els de 4 o més caràcters. 
# ---------------------------
# processar argument
for arg in $*
do
	echo "$arg" | grep -E ".{4,}$"
done
exit 0
