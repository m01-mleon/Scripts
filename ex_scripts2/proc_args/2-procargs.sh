#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 2-Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters. 
# --------------------------- 
comptador=0
for arg in $*
do
       	echo "$arg" | grep -q -E "[^.*]{3,}"
	if [ $? -eq 0 ];then
		((comptador++))
	fi
done
echo $comptador
exit 0
