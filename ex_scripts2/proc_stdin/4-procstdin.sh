#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 4-Processar stdin mostrant per stdout les línies numerades i en majúscules.
# --------------------------- 
OK=0
count=0
while read -r line
do
	((count++))
	echo $cont $line | tr '[:lower:]' '[:upper:]'
done
exit $OK
