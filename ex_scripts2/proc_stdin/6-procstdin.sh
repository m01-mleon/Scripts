#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 6-Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia
# en format → T. Snyder.
# ---------------------------
OK=0
while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d' ' -f2 )
	echo $nom. $cognom
done
exit $OK
