#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 5-Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# --------------------------- 
OK=0
while read -r line
do
	chars=$(echo $line | wc -c)
	if [ $chars -lt 50 ];then
		echo $line
	fi
done
exit $OK
