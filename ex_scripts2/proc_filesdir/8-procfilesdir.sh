#! /bin/bash
# Marcos León Correa
# 26/02/2024
#
# Exercicis d'scripts 2
# 8-Programa: prog file...
# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout
# el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per
# stderror si no s’ha pogut comprimir. En finalitzar es mostra per stdout quants files
# ha comprimit.
# Retorna status 0 ok, 1 error no args, 2 si algun error en comprimir.
# b) Ampliar amb el cas: prog -h|--help.
# --------------------------- 
