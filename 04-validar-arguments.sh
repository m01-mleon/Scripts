#! /bin/bash
# Marcos León Correa
# Febrer 2024
#
# Validar arguments
# --------------------
# 1) Valida que hi ha 2 args
if [ $# -ne 2 ]
then
      	echo "num args invalid: Usage $0 nom cognom"
	exit 1
fi
# 2)Xixa
echo "nom: $1"
echo "cognom: $2"
exit 0
