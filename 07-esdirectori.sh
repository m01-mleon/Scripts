#! /bin/bash
# Marcos León Correa
# Febrer 2024
#
# Llistar directori rebut
# -Verificar rep arg
# -Verificar que es un dir
# -------------------------
ERR_ARGS=1
ERR_NODIR=2
# 1) requisits arg
if [ $# -ne 1 ]
then
	echo "err $1 arg invalid, usage: $0 'rutadir'"
	exit $ERR_ARGS
fi
# 2) requisit dir
if ! [ -d $1 ]
then
	echo "err $1 arg invalid, usage: $0 'rutadir'"
	exit $ERR_NODIR
fi
#3) xixa
ls $1
exit 0
