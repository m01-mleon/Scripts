#! /bin/bash
# Marcos León Correa
# Febrer 2024
# case diames: dir els dies que te un mes
# prog mes:
# 	a) validar rep un arg
# 	b) validar mes [1-12]
# 	c) xixa
# ---------------------
# 1) validar arg
ERR_ARG=1
ERR_DIA=2
if [ $# -ne 1 ]; then
	echo "Error $1 invalid arg, Usage: $0 <1-12> "
	exit $ERR_ARG
fi
# 2) validar mes

if ! [ $1 -ge 1 -a $1 -lt 13 ]; then
	echo "Error $1 mes no valid, Usage $0 <1-12>"
	exit $ERR_DIA
fi
# 3) xixa
case $1 in
	"2")
		dies=28;;
	"4"|"6"|"9"|"11")
		dies=30;;
	*)
		dies=31;;
esac
echo "El mes $mes te $dies dies"
exit 0
