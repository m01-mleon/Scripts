#! /bin/bash
# Marcos León Correa
# Febrer 2024
# exemples for
# ---------------------
# 8)
logins=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $logins
do
	echo "$num: $elem"
	num=$((num+1))
done
exit 0
# 7) llistar numerant les linies
llistat=$(ls)
num=1
for elem in $llistat
do
	echo "$num: $elem"
	num=$((num+1))
done
exit 0
# 6) iterar per cada un dels valor que genera la ordre ls
llistat=$(ls)
num=1
for elem in $llistat
do
	echo "$elem"
done
exit 0
# 5) numerar arrgs
num=1
for arg in $*
do
	echo "$num: $arg"
	num=$((num+1))
done
exit 0
# 4) $@ expandeix $* no
for arg in "$@"
do
	echo "$arg"
done
exit 0 
# 3) iterar per la llista d'args
for arg in "$*"
do
	echo "$arg"
done
exit 0
# 2) iterar noms
for nom in pere marta anna pau
do
	echo "$nom"
done
exit 0 
# 1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0 

