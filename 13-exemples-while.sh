#! /bin/bash
# Marcos León Correa
# Febrer 2024
# exemples while
# -------------------
# 8) processar linia linia un fitxer
fitxer=$1
i=1
while read -r line
do
	chars=$(echo $line | wc -c)
	echo "$i: ($chars) $line" | tr 'a-z' 'A-Z'
	((i++))
done < $fitxer
exit 0
# 7) numerar i mostrar en majus
i=1
while read -r line
do
	echo "$i: $line" | tr 'a-z' 'A-Z'
	((i++))
done
exit 0
# 6) itera linia linia fins token
TOKEN="FI"
i=1
read -r line
while [ $line != $TOKEN ]
do
	echo "$i: $line"
	((i++))
	read -r line
done
exit 0
# 5)numerar les linies rebudes
i=1
while read -r line 
do
	echo "$i: $line"
	((i++))
done
exit 0
# 4) 
# while 'read -r $' ... do...done
# PROCESSA LINIA A LINIA L'ENTRADA STANDARD

while read -r line 
do
	echo $line
done
exit 0 
# 3) iterar lista d'arguments

while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift 
done
exit 0
# 2) comptador decrementa valor rebut
MIN=0
num=$1
while [ $num -ge $MIN ]
do
	echo -n "$num"
	((num--))
done
exit 0
# 1) mostrar numeros del 1 al 10
MAX=10
num=1
while  [ $num -le $MAX ]
do
	echo -n "$num"
	((num++))
done
exit 0
